#ifndef DATASECTION_H
#define DATASECTION_H

#include "ast.h"
#include <boost/spirit/home/x3.hpp>

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	struct data_section_class;
	using data_section_type = x3::rule<data_section_class, ast::data_section>;

	BOOST_SPIRIT_DECLARE(data_section_type);

	data_section_type const& data_section_parser(void);
} // namespace parser
} // namespace roptool

#endif // DATASECTION_H
