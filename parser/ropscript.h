#ifndef ROPSCRIPT_H
#define ROPSCRIPT_H

#include "ast.h"
#include <boost/spirit/home/x3.hpp>

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	struct ropscript_class;
	using ropscript_type = x3::rule<ropscript_class, ast::ropscript>;

	BOOST_SPIRIT_DECLARE(ropscript_type);

	ropscript_type const& ropscript_parser(void);
} // namespace parser
} // namespace roptool

#endif // ROPSCRIPT_H
