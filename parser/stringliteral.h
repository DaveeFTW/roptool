#ifndef STRINGLITERAL_H
#define STRINGLITERAL_H

#include "ast.h"
#include <boost/spirit/home/x3.hpp>

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	struct string_literal_class;
	using string_literal_type = x3::rule<string_literal_class, roptool::ast::string_literal>;
	
	BOOST_SPIRIT_DECLARE(string_literal_type);
	string_literal_type const& string_literal_parser(void);
} // namespace parser
} // namespace roptool

#endif // STRINGLITERAL_H
