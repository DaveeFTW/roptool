#ifndef ASLR_H
#define ASLR_H

#include "ast.h"
#include <boost/spirit/home/x3.hpp>

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;

	using aslr_type = x3::rule<class aslr_class, ast::aslr_decl>;

	BOOST_SPIRIT_DECLARE(aslr_type);

	aslr_type const& aslr_parser(void);
} // namespace parser
} // namespace roptool

#endif // ASLR_H
