#ifndef SKIPPER_H
#define SKIPPER_H

#include "ast.h"
#include <boost/spirit/home/x3.hpp>

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	struct skipper_class;
	using skipper_type = x3::rule<skipper_class>;

	BOOST_SPIRIT_DECLARE(skipper_type);

	skipper_type const& skipper_parser(void);
} // namespace parser
} // namespace roptool

#endif // SKIPPER_H
