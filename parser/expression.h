#ifndef EXPRESSION_H
#define EXPRESSION_H

#include "ast.h"
#include <boost/spirit/home/x3.hpp>

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	struct expression_class;
	using expression_type = x3::rule<expression_class, ast::expression>;
	BOOST_SPIRIT_DECLARE(expression_type);
	
	expression_type const& expression_parser(void);
} // namespace parser
} // namespace roptool

#endif // EXPRESSION_H
