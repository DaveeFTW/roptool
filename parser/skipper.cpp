#include "skipper.h"
#include "parserconfig.h"
#include "boosthelper.h"

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	struct line_comment_class;
	struct block_comment_class;

	using line_comment_type = x3::rule<line_comment_class>;
	using block_comment_type = x3::rule<block_comment_class>;
	
	line_comment_type const line_comment = "line_comment";	
	block_comment_type const block_comment = "block_comment";	
	skipper_type const skipper = "skipper";

	auto const line_comment_def = x3::lit("//") >> *(x3::char_ - x3::eol) >> x3::eol;
	auto const block_comment_def = x3::lit("/*") >> *(x3::char_ - "*/") >> x3::lit("*/");
	auto const skipper_def = x3::ascii::space | line_comment | block_comment;
	
	SPIRIT_DEFINE_AUTO(skipper, line_comment, block_comment);	
	
	// parser exposure
	skipper_type const& skipper_parser(void) { return skipper; }
	
	BOOST_SPIRIT_INSTANTIATE(skipper_type, iterator_type, context_type);
} // namespace parser
} // namespace roptool