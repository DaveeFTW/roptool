#include "number.h"
#include "parserconfig.h"

#include "boosthelper.h"
#include <boost/spirit/home/x3.hpp>

#include <cstdint>

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	using x3::char_;
	using x3::lit;

	// integer parsers
    x3::uint_parser<ast::u64, 10> const p_dec64;
    x3::uint_parser<ast::u32, 10> const p_dec32;
    x3::uint_parser<ast::u16, 10> const p_dec16;
    x3::uint_parser<ast::u8, 10> const p_dec8;

    x3::uint_parser<ast::u64, 16, 1, 16> const p_hex64;
    x3::uint_parser<ast::u32, 16, 1, 8> const p_hex32;
    x3::uint_parser<ast::u16, 16, 1, 4> const p_hex16;
    x3::uint_parser<ast::u8, 16, 1, 2> const p_hex8;

    x3::uint_parser<ast::u64, 8> const p_oct64;
    x3::uint_parser<ast::u32, 8> const p_oct32;
    x3::uint_parser<ast::u16, 8> const p_oct16;
    x3::uint_parser<ast::u8, 8> const p_oct8;
	
	// decimal
	dec64_type const dec64 = "dec64";
	dec32_type const dec32 = "dec32";
	dec16_type const dec16 = "dec16";
	dec8_type const dec8 = "dec8";

	auto const dec64_def = p_dec64;
	auto const dec32_def = p_dec32;
	auto const dec16_def = p_dec16;
	auto const dec8_def  = p_dec8;

	// hex
	hex64_type const hex64 = "hex64";
	hex32_type const hex32 = "hex32";
	hex16_type const hex16 = "hex16";
	hex8_type const hex8 = "hex8";

	auto const hex64_def = (lit("0x") | lit("0X")) > p_hex64;
	auto const hex32_def = (lit("0x") | lit("0X")) > p_hex32;
	auto const hex16_def = (lit("0x") | lit("0X")) > p_hex16;
	auto const hex8_def  = (lit("0x") | lit("0X")) > p_hex8;

	// octal
	oct64_type const oct64 = "oct64";
	oct32_type const oct32 = "oct32";
	oct16_type const oct16 = "oct16";
	oct8_type const oct8 = "oct8";

	auto const oct64_def = lit('0') >> p_oct64;
	auto const oct32_def = lit('0') >> p_oct32;
	auto const oct16_def = lit('0') >> p_oct16;
	auto const oct8_def  = lit('0') >> p_oct8;
	
	// generic
	qword_type const qword = "qword";
	dword_type const dword = "dword";
	word_type const word = "word";
	byte_type const byte = "byte";

	auto const qword_def = hex64 | oct64 | dec64;
	auto const dword_def = hex32 | oct32 | dec32;
	auto const word_def = hex16 | oct16 | dec16;
	auto const byte_def = hex8 | oct8 | dec8;

	SPIRIT_DEFINE_AUTO(
		  qword
		, dword
		, word
		, byte
		, hex64
		, hex32
		, hex16
		, hex8
		, dec64
		, dec32
		, dec16
		, dec8
		, oct64
		, oct32
		, oct16
		, oct8
	);
	
	// parser exposure
	qword_type const& qword_parser(void) { return qword; }
	dword_type const& dword_parser(void) { return dword; }
	word_type const& word_parser(void) { return word; }
	byte_type const& byte_parser(void) { return byte; }
	hex64_type const& hex64_parser(void) { return hex64; }
	hex32_type const& hex32_parser(void) { return hex32; }
	hex16_type const& hex16_parser(void) { return hex16; }
	hex8_type const& hex8_parser(void) { return hex8; }
	dec64_type const& dec64_parser(void) { return dec64; }
	dec32_type const& dec32_parser(void) { return dec32; }
	dec16_type const& dec16_parser(void) { return dec16; }
	dec8_type const& dec8_parser(void) { return dec8; }
	oct64_type const& oct64_parser(void) { return oct64; }
	oct32_type const& oct32_parser(void) { return oct32; }
	oct16_type const& oct16_parser(void) { return oct16; }
	oct8_type const& oct8_parser(void) { return oct8; }
	
	BOOST_SPIRIT_INSTANTIATE(qword_type, iterator_type, context_type);
	BOOST_SPIRIT_INSTANTIATE(dword_type, iterator_type, context_type);
	BOOST_SPIRIT_INSTANTIATE(word_type, iterator_type, context_type);
	BOOST_SPIRIT_INSTANTIATE(byte_type, iterator_type, context_type);
	BOOST_SPIRIT_INSTANTIATE(hex64_type, iterator_type, context_type);
	BOOST_SPIRIT_INSTANTIATE(hex32_type, iterator_type, context_type);
	BOOST_SPIRIT_INSTANTIATE(hex16_type, iterator_type, context_type);
	BOOST_SPIRIT_INSTANTIATE(hex8_type, iterator_type, context_type);
	BOOST_SPIRIT_INSTANTIATE(dec64_type, iterator_type, context_type);
	BOOST_SPIRIT_INSTANTIATE(dec32_type, iterator_type, context_type);
	BOOST_SPIRIT_INSTANTIATE(dec16_type, iterator_type, context_type);
	BOOST_SPIRIT_INSTANTIATE(dec8_type, iterator_type, context_type);
	BOOST_SPIRIT_INSTANTIATE(oct64_type, iterator_type, context_type);
	BOOST_SPIRIT_INSTANTIATE(oct32_type, iterator_type, context_type);
	BOOST_SPIRIT_INSTANTIATE(oct16_type, iterator_type, context_type);
	BOOST_SPIRIT_INSTANTIATE(oct8_type, iterator_type, context_type);
} // namespace parser
} // namespace roptool