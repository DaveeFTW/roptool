#ifndef ASTADAPTED_H
#define ASTADAPTED_H

#include "ast.h"
#include <boost/fusion/adapted/struct.hpp>
#include <boost/fusion/include/vector.hpp>

BOOST_FUSION_ADAPT_STRUCT(
	roptool::ast::namespace_identifier,
	(roptool::ast::identifier, namespace_)
	(roptool::ast::identifier, name)
)

BOOST_FUSION_ADAPT_STRUCT(
	roptool::ast::unary_operand,
	(roptool::ast::unary_operator, operator_)
	(roptool::ast::operand, operand_)
)

BOOST_FUSION_ADAPT_STRUCT(
	roptool::ast::operation,
	(roptool::ast::expression_operator, operator_)
	(roptool::ast::operand, operand_)
)

BOOST_FUSION_ADAPT_STRUCT(
	roptool::ast::expression,
	(roptool::ast::operand, first)
	(std::list<roptool::ast::operation>, rest)
)

BOOST_FUSION_ADAPT_STRUCT(
	roptool::ast::function_call,
	unused,
	name,
	arguments
)

BOOST_FUSION_ADAPT_STRUCT(
	roptool::ast::buffer_decl,
	unused,
	name,
	data
)

BOOST_FUSION_ADAPT_STRUCT(
	roptool::ast::buffer_data,
	unused,
	size,
	data
)

BOOST_FUSION_ADAPT_STRUCT(
	roptool::ast::symbol_decl,
	unused,
	name,
	value
)

BOOST_FUSION_ADAPT_STRUCT(
	roptool::ast::functiondef_decl,
	unused,
	name,
	value
)

BOOST_FUSION_ADAPT_STRUCT(
	roptool::ast::variable_decl,
	unused,
	name,
	value
)

BOOST_FUSION_ADAPT_STRUCT(
	roptool::ast::data_section,
	namespace_, entries
)

BOOST_FUSION_ADAPT_STRUCT(
	roptool::ast::code_section,
	unused,
	identifier,
	parameters,
	statements
)

BOOST_FUSION_ADAPT_STRUCT(
	roptool::ast::return_argument,
	name
)

BOOST_FUSION_ADAPT_STRUCT(
	roptool::ast::load_argument,
	unused,
	expr
)

BOOST_FUSION_ADAPT_STRUCT(
	roptool::ast::aslr_decl,
	unused,
	name
)

#endif // ASTADAPTED_H
