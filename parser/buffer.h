#ifndef BUFFER_H
#define BUFFER_H

#include "ast.h"
#include <boost/spirit/home/x3.hpp>

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	struct buffer_class;
	struct namespaced_identifier_class;
	
	using buffer_type = x3::rule<buffer_class, ast::buffer_decl>;

	BOOST_SPIRIT_DECLARE(buffer_type);

	buffer_type const& buffer_parser(void);
} // namespace parser
} // namespace roptool

#endif // BUFFER_H
