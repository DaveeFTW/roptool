#include "parserconfig.h"

namespace
{
	roptool::parser::bit_length anon_bit_length = roptool::parser::bit_length::_32; 
} // anonoymous

namespace roptool { namespace parser 
{
	void set_architecture_abi_len(bit_length length)
	{
		anon_bit_length = length;
	}
	
	bit_length architecture_abi_len(void)
	{
		return anon_bit_length;
	}
} // namespace parser
} // namespace roptool
