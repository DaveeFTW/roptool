#ifndef PARAMETER_H
#define PARAMETER_H

#include "boosthelper.h"
#include <boost/spirit/home/x3.hpp>

#include <cstdint>

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	struct byte_class;
	struct word_class;
	struct dword_class;
	struct qword_class;
	
	using byte_type = x3::rule<byte_class, ast::parameter>;
	using word_type = x3::rule<word_class, ast::parameter>;
	using dword_type = x3::rule<dword_class, ast::parameter>;
	using qword_type = x3::rule<qword_class, ast::parameter>;
	
	byte_type const byte = "byte";
	word_type const word = "word";
	dword_type const dword = "dword";
	qword_type const qword = "qword";
	
	auto const expression_param_def = expression;
	auto const string_param_def = quoted_string;
	auto const load_param_def = x3::char_('[') > expression > x3::char_(']');
	auto const return_param_def = x3::char_('R') > x3::char_('E') > x3::char_('T');
	auto const type_param_def = quoted_string | load_param | return_param;
	auto const byte_param_def = byte | type_param;
	auto const word_param_def = word | type_param;
	auto const dword_param_def = dword | type_param;
	auto const qword_param_def = qword | type_param;
	
	auto const parameter_def = () | (x3::attr(32) > type_param)
	
	auto const byte_def = x3::lit("BYTE") > x3::char_('(') > byte
	auto const letter_def = x3::char_("a-zA-Z_");
	auto const digit_def = x3::char_("0-9");
	auto const identifier_def = x3::lexeme[letter >> *(letter | decimal_digit)];
	
	SPIRIT_DEFINE_AUTO(
		  letter
		, digit
		, identifier
	);	
} // namespace parser
} // namespace roptool

#endif // PARAMETER_H
