#include "symbol.h"
#include "stringliteral.h"
#include "expression.h"
#include "identifier.h"
#include "astadapted.h"
#include "parserconfig.h"
#include "boosthelper.h"

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;

	symbol_type const symbol = "symbol";

	auto const symbol_def =
	x3::lit("symbol")
	>> x3::attr(false) >> identifier_parser() // x3::attr hack for 1.61 compatibility
	>> x3::lit('=')
	>> (expression_parser() | string_literal_parser())
	;

	SPIRIT_DEFINE_AUTO(symbol);

	// parser exposure
	symbol_type const& symbol_parser(void) { return symbol; }

	BOOST_SPIRIT_INSTANTIATE(symbol_type, iterator_type, context_type);
} // namespace parser
} // namespace roptool
