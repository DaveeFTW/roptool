#ifndef NUMBER_H
#define NUMBER_H

#include "ast.h"
#include <boost/spirit/home/x3.hpp>

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	using x3::char_;
	using x3::lit;

	// decimal
	struct dec64_class;
	struct dec32_class;
	struct dec16_class;
	struct dec8_class;

	using dec64_type = x3::rule<dec64_class, ast::u64>;
	using dec32_type = x3::rule<dec32_class, ast::u32>;
	using dec16_type = x3::rule<dec16_class, ast::u16>;
	using dec8_type = x3::rule<dec8_class, ast::u8>;

	// hexadecimal
	struct hex64_class;
	struct hex32_class;
	struct hex16_class;
	struct hex8_class;

	using hex64_type = x3::rule<hex64_class, ast::u64>;
	using hex32_type = x3::rule<hex32_class, ast::u32>;
	using hex16_type = x3::rule<hex16_class, ast::u16>;
	using hex8_type = x3::rule<hex8_class, ast::u8>;

	// octal
	struct oct64_class;
	struct oct32_class;
	struct oct16_class;
	struct oct8_class;

	using oct64_type = x3::rule<oct64_class, ast::u64>;
	using oct32_type = x3::rule<oct32_class, ast::u32>;
	using oct16_type = x3::rule<oct16_class, ast::u16>;
	using oct8_type = x3::rule<oct8_class, ast::u8>;

	// generic
	struct qword_class;
	struct dword_class;
	struct word_class;
	struct byte_class;

	using qword_type = x3::rule<qword_class, ast::u64>;
	using dword_type = x3::rule<dword_class, ast::u32>;
	using word_type = x3::rule<word_class, ast::u16>;
	using byte_type = x3::rule<byte_class, ast::u8>;

	BOOST_SPIRIT_DECLARE(
		  qword_type
		, dword_type
		, word_type
		, byte_type
		, hex64_type
		, hex32_type
		, hex16_type
		, hex8_type
		, dec64_type
		, dec32_type
		, dec16_type
		, dec8_type
		, oct64_type
		, oct32_type
		, oct16_type
		, oct8_type
	);

	qword_type const& qword_parser(void);
	dword_type const& dword_parser(void);
	word_type const& word_parser(void);
	byte_type const& byte_parser(void);
	hex64_type const& hex64_parser(void);
	hex32_type const& hex32_parser(void);
	hex16_type const& hex16_parser(void);
	hex8_type const& hex8_parser(void);
	dec64_type const& dec64_parser(void);
	dec32_type const& dec32_parser(void);
	dec16_type const& dec16_parser(void);
	dec8_type const& dec8_parser(void);
	oct64_type const& oct64_parser(void);
	oct32_type const& oct32_parser(void);
	oct16_type const& oct16_parser(void);
	oct8_type const& oct8_parser(void);
	
} // namespace parser
} // namespace roptool

#endif // NUMBER_H
