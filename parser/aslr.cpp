#include "aslr.h"
#include "identifier.h"
#include "astadapted.h"
#include "parserconfig.h"
#include "boosthelper.h"

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;

	aslr_type const aslr = "aslr";

	auto const aslr_def = aslr_type{ "aslr" } =
	x3::lit("aslr")
	>> x3::attr(false) >> identifier_parser() // x3::attr hack for 1.61 compatibility
	;

	SPIRIT_DEFINE_AUTO(aslr);

	// parser exposure
	aslr_type const& aslr_parser(void) { return aslr; }

	BOOST_SPIRIT_INSTANTIATE(aslr_type, iterator_type, context_type);
} // namespace parser
} // namespace roptool
