#include "codesection.h"
#include "identifier.h"
#include "expression.h"
#include "functioncall.h"
#include "astadapted.h"
#include "parserconfig.h"
#include "boosthelper.h"
#include "annotation.h"

#include <iostream>

namespace roptool { namespace parser
{
    namespace x3 = boost::spirit::x3;

    code_section_type const code_section = "code_section";

    auto const code_section_def = code_section_type{} %=
    x3::lit("code")
    > x3::lit(':')
    >> x3::attr(false) >> identifier_parser()  // x3::attr hack for 1.61 compatibility
    >> -(x3::lit('(') > -((expression_parser() | (x3::lit('&') > identifier_parser())) % x3::lit(',')) > x3::lit(')'))
    >> x3::lit('{')
    >> *((function_call_parser()
    | return_statement_parser())
    > 	+x3::lit(';'))
    >> x3::lit('}')
    ;

    SPIRIT_DEFINE_AUTO(code_section);

    // parser exposure
    code_section_type const& code_section_parser(void) { return code_section; }

    BOOST_SPIRIT_INSTANTIATE(code_section_type, iterator_type, context_type);

    struct code_section_class : annotation_base {};
} // namespace parser
} // namespace roptool
