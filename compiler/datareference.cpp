#include "DataRef.h"

DataReference::DataReference(u64 init, bool constant)
	: m_value(init)
	, m_constant(constant)
{
	m_operation_handlers =
	{
		{ DataReferenceOperation::ADDITION, [](const auto x, const auto y) { return x + y; } },
		{ DataReferenceOperation::SUBTRACTION, [](const auto x, const auto y) { return x + y; } },
		{ DataReferenceOperation::MULTIPLICATION, [](const auto x, const auto y) { return x * y; } },
		{ DataReferenceOperation::DIVISION, [](const auto x, const auto y) { return x / y; } },
	};
	
	m_unary_operation_handlers =
	{
		{ DataReferenceUnaryOperation::NEGATIVE, [](const auto x) { return (u64)0-x; } },
		{ DataReferenceUnaryOperation::ONES_COMPLEMENT, [](const auto x) { return ~x; } },
		{ DataReferenceUnaryOperation::EQUAL, [](const auto x) { return x; } },
	};
}

u64 DataReference::get(void) const
{
	return m_value;
}

void DataReference::set(Operand rhs, DataReferenceUnaryOperation op)
{
    if (m_constant)
    {
        // TODO: assert?
        return;
    }
    
    auto ref = rhs.which() == 0 ? std::make_shared<DataReference>(boost::get<u64>(rhs), true) : boost::get<DataRefPtr>(rhs);

    // TODO: setup links etc
    if (ref->constant())
    {
        m_value = m_unary_operation_handlers[op](ref->get());
        set_constant();
    }
    else
    {
        // not constant, so we need to track it
        m_op = op;
        m_operand = ref;
        m_operand->on_update(this);
    }
    
    recalculate();
}


void DataReference::set(Operand op1_, Operand op2_, DataReferenceOperation op)
{
    if (m_constant)
    {
        // TODO: assert?
        return;
    }
    
    auto op1 = op1_.which() == 0 ? std::make_shared<DataReference>(boost::get<u64>(op1_), true) : boost::get<DataRefPtr>(op1_);
    auto op2 = op2_.which() == 0 ? std::make_shared<DataReference>(boost::get<u64>(op2_), true) : boost::get<DataRefPtr>(op2_);

    if (op1->constant() && op2->constant())
    {
        m_value = m_operation_handlers[op](op1->get(), op2->get());
        set_constant();
    }
    else
    {     
        // not constant, so we need to track it 
        m_op = op;  
        m_operand = op1;
        m_operand2 = op2;
        
        if (!m_operand->constant())
            m_operand->on_update(this);
        
        if (!m_operand2->constant())
            m_operand2->on_update(this);
    }
    
    recalculate();
}

bool DataReference::constant(void) const
{
	return m_constant;
}

DataReference::Graph* DataReference::to_graph(void)
{
    Graph *g = new Graph;
    
    auto v0 = boost::add_vertex(*g);
    (*g)[v0].ref = this;
    
    add_tree(g, this, v0);
    return g;
}

void DataReference::recalculate(void)
{
    u64 comp = 0;
    
    if (m_constant)
        return;
    
    switch(m_op.which())
    {
        case 0: // DataReferenceOperation
        {
            auto op = boost::get<DataReferenceOperation>(m_op);
            comp = m_operation_handlers[op](m_operand->get(), m_operand2->get());
            break;
        }
        
        case 1: // DataReferenceUnaryOperation
        {
            auto op = boost::get<DataReferenceUnaryOperation>(m_op);
            comp = m_unary_operation_handlers[op](m_operand->get());
            break;
        }
        
        default: // unknown
        {
            BOOST_ASSERT(0);
            break;
        }
    }
    
    // check if we're now constant value
    if (m_operand->constant() && m_operand2->constant())
    {
        m_value = comp;
        set_constant();
    }
    
    else if (comp != m_value)
    {
        // notify our subscribers
        for (auto& sub : m_subscribers)
            sub->recalculate();
        
        m_value = comp;
    }
}

void DataReference::set_constant(void)
{
	m_constant = true;
    
    // notify our subscribers
    for (auto& sub : m_subscribers)
        sub->recalculate();

    m_subscribers.empty();
}

void DataReference::on_update(DataReference *ref)
{
    if (m_constant) // ASSERT?
        return;
    
    m_subscribers.push_back(ref);
}

void DataReference::add_tree(Graph *g, DataReference *ref, boost::graph_traits<Graph>::vertex_descriptor v0)
{
    (*g)[v0].own = false;
    
    if (!ref->constant() && ref->m_operand)
    {
        (*g)[v0].op = ref->m_op;
        
        switch(ref->m_op.which())
        {
            case 0: // DataReferenceOperation
            {
                auto v1 = boost::add_vertex(*g);
                (*g)[v1].ref = ref->m_operand.get();
                
                auto v2 = boost::add_vertex(*g);
                (*g)[v2].ref = ref->m_operand2.get();
                
                auto e1 = boost::add_edge(v0, v1, *g);
                (*g)[e1.first].side = DataReference::LEFT;
                
                auto e2 = boost::add_edge(v0, v2, *g);
                (*g)[e2.first].side = DataReference::RIGHT;
                
                add_tree(g, ref->m_operand.get(), v1);
                add_tree(g, ref->m_operand2.get(), v2);
                break;
            }
            
            case 1: // DataReferenceUnaryOperation
            {
                auto v1 = boost::add_vertex(*g);
                (*g)[v1].ref = ref->m_operand.get();
                
                auto e1 = boost::add_edge(v0, v1, *g);
                (*g)[e1.first].side = DataReference::LEFT;
                
                add_tree(g, ref->m_operand.get(), v1);
                break;
            }
            
            default: // unknown
            {
                BOOST_ASSERT(0);
                break;
            }
        }    
    }
}
