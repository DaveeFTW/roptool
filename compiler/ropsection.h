#ifndef ROPSECTION_H
#define ROPSECTION_H

// roptool
#include "ropfunction.h"

// std
#include <memory>

class RopSection
{
public:
    bool add(const RopFunctionPtr& func);
    RopFunctionPtrList functions(void);

private:
    RopFunctionPtrList m_funcs;
};

typedef std::shared_ptr<RopSection> RopSectionPtr;

#endif // ROPSECTION_H
