#ifndef _TARGET_H_
#define _TARGET_H_

// roptool
#include "targetmanifest.h"
#include "gadgetmap.h"
#include "DataRef.h"

// std
#include <string>
#include <memory>
#include <map>

using AslrTable = std::map<std::string, DataRefPtr>;

class Target
{
	public:
		virtual const std::string& name(void) = 0;
		virtual void setName(const std::string& name) = 0;
		virtual TargetManifestPtr manifest(void) = 0;

		virtual GadgetMapPtr bestGadgetMap(const std::string& regex) = 0;

		virtual GadgetPtr getCallerGadget(void) = 0;

		virtual bool isFunction(const std::string& function) const = 0;

		virtual const AslrTable& aslr_table(void) const = 0;

		virtual AslrTable& aslr_table(void) = 0;
};

typedef std::shared_ptr<Target> TargetPtr;

#endif // _TARGET_H_
