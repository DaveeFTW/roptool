#include "relocation.h"

#include <boost/iterator/filter_iterator.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <iostream>

namespace {


void analyse_precedence(DataReference::OperationType op_, bool& is_additive, bool& is_multiplicative, bool& is_unary)
{
    is_additive = false;
    is_multiplicative = false;
    is_unary = false;

    switch (op_.which())
    {
        case 0: // DataReferenceOperation
        {
            auto op = boost::get<DataReferenceOperation>(op_);

            switch (op)
            {
                case DataReferenceOperation::ADDITION:
                case DataReferenceOperation::SUBTRACTION:
                    is_additive = true;
                    break;
                case DataReferenceOperation::MULTIPLICATION:
                case DataReferenceOperation::DIVISION:
                    is_multiplicative = true;
                    break;
            }
            break;
        }

        case 1: // DataReferenceUnaryOperation
        {
            auto op = boost::get<DataReferenceUnaryOperation>(op_);
            is_unary = true;
            switch (op)
            {
                case DataReferenceUnaryOperation::NEGATIVE:
                    break;
                case DataReferenceUnaryOperation::EQUAL:
                    break;
                case DataReferenceUnaryOperation::ONES_COMPLEMENT:
                    break;
            }

            break;
        }

        default: // unknown
        {
            BOOST_ASSERT(0);
            break;
        }
    }
}

bool shared_precedence(DataReference::OperationType op1, DataReference::OperationType op2)
{
    bool op1_additive, op1_multiplicative, op1_unary;
    bool op2_additive, op2_multiplicative, op2_unary;

    analyse_precedence(op1, op1_additive, op1_multiplicative, op1_unary);
    analyse_precedence(op2, op2_additive, op2_multiplicative, op2_unary);

    if (op1_additive && op2_additive)
        return true;

    if (op1_multiplicative && op2_multiplicative)
        return true;

    // TODO: unary...
    return false;
}

bool is_left_node(DataReference::VertexDescriptor parent, DataReference::VertexDescriptor node, DataReference::Graph *graph)
{
    auto e = boost::edge(parent, node, *graph);
    BOOST_ASSERT(e.second);
    return (*graph)[e.first].side == DataReference::LEFT;
}

bool is_operation(DataReference::OperationType op, DataReference::OperationType op2)
{
    return op == op2;
}

void move_to_left(DataReference::VertexDescriptor parent, DataReference::VertexDescriptor node, DataReference::Graph *graph)
{
    auto e = boost::edge(parent, node, *graph);
    BOOST_ASSERT(e.second);
    (*graph)[e.first].side = DataReference::LEFT;
}

DataReference::VertexDescriptor get_sibling(DataReference::VertexDescriptor parent, DataReference::VertexDescriptor node, DataReference::Graph *graph)
{
    using OutEdgeIterator = DataReference::GraphTraits::out_edge_iterator;

    OutEdgeIterator out_i, out_end;
    for (boost::tie(out_i, out_end) = boost::out_edges(parent, *graph); out_i != out_end; ++out_i)
    {
        auto child = boost::target(*out_i, *graph);

        if (child != node)
        {
            return child;
        }
    }

    BOOST_ASSERT(0);
    while (1);
}

bool promote_constant(DataReference::VertexDescriptor parent, DataReference::VertexDescriptor vertex, DataReference::VertexDescriptor constant, DataReference::Graph *graph)
{
    using OutEdgeIterator = DataReference::GraphTraits::out_edge_iterator;

    // check that both types can interact
    if (!shared_precedence((*graph)[vertex].op, (*graph)[parent].op))
        return false;

    // add constant to parent if possible
    OutEdgeIterator out_i, out_end;
    for (boost::tie(out_i, out_end) = boost::out_edges(parent, *graph); out_i != out_end; ++out_i)
    {
        auto child = boost::target(*out_i, *graph);

        if ((*graph)[child].ref->constant())
        {
            auto child_lhs = is_left_node(vertex, constant, graph);
            auto parent_lhs = is_left_node(parent, child, graph);

            auto child_lhs_ref = std::make_shared<DataReference>(child_lhs ? (*graph)[constant].ref->get() : 0, true);
            auto child_rhs_ref = std::make_shared<DataReference>(!child_lhs ? (*graph)[constant].ref->get() : 0, true);
            auto child_eqn = std::make_shared<DataReference>();
            child_eqn->set(child_lhs_ref, child_rhs_ref, boost::get<DataReferenceOperation>((*graph)[vertex].op));

            auto parent_lhs_ref = parent_lhs ? std::make_shared<DataReference>((*graph)[child].ref->get(), true) : child_eqn;
            auto parent_rhs_ref = !parent_lhs ? std::make_shared<DataReference>((*graph)[child].ref->get(), true) : child_eqn;
            auto eqn = std::make_shared<DataReference>();
            eqn->set(parent_lhs_ref, parent_rhs_ref, boost::get<DataReferenceOperation>((*graph)[parent].op));

            if ((*graph)[child].own)
                delete (*graph)[child].ref;

            (*graph)[child].ref = new DataReference(eqn->get(), true);
            (*graph)[child].own = true;

            if (is_operation((*graph)[vertex].op, DataReferenceOperation::SUBTRACTION))
            {
                (*graph)[vertex].op = DataReferenceUnaryOperation::NEGATIVE;
            }
            else
            {
                (*graph)[vertex].op = DataReferenceUnaryOperation::EQUAL;
            }

            if (child_lhs)
            {
                move_to_left(vertex, get_sibling(vertex, constant, graph), graph);
            }

            boost::clear_vertex(constant, *graph);
            boost::remove_vertex(constant, *graph);
            return true;
        }
    }

    return false;
}

bool optimise_unary(DataReference::VertexDescriptor parent, DataReference::VertexDescriptor vertex, DataReference::Graph *graph)
{
    using OutEdgeIterator = DataReference::GraphTraits::out_edge_iterator;

    // this must be DataReferenceUnaryOperation
    BOOST_ASSERT((*graph)[vertex].op.which() == 1);

    auto op = boost::get<DataReferenceUnaryOperation>((*graph)[vertex].op);

    switch (op)
    {
        case DataReferenceUnaryOperation::NEGATIVE:
            if (is_operation((*graph)[parent].op, DataReferenceOperation::ADDITION))
            {
                // ensure other paremeter is on left
                if (is_left_node(parent, vertex, graph))
                {
                    move_to_left(parent, get_sibling(parent, vertex, graph), graph);
                }

                OutEdgeIterator out_i, out_end;
                boost::tie(out_i, out_end) = boost::out_edges(vertex, *graph);
                BOOST_ASSERT(std::distance(out_i, out_end) == 1);

                auto value = boost::target(*out_i, *graph);
                auto e_old = boost::edge(parent, vertex, *graph);
                BOOST_ASSERT(e_old.second);

                auto e = boost::add_edge(parent, value, *graph);
                (*graph)[e.first].side = DataReference::RIGHT;

                boost::clear_vertex(vertex, *graph);
                boost::remove_vertex(vertex, *graph);

                // change parent from addition to subtraction
                (*graph)[parent].op = DataReferenceOperation::SUBTRACTION;
                return true;
            }
            else if (is_operation((*graph)[parent].op, DataReferenceOperation::SUBTRACTION))
            {
                // if we're on the left, we need other side to be constant and negate it
                auto lhs = is_left_node(parent, vertex, graph);

                if (lhs)
                {
                    auto sibling = get_sibling(parent, vertex, graph);

                    if (!(*graph)[sibling].ref->constant())
                    {
                        break;
                    }

                    auto neg_constant = new DataReference(0-(*graph)[sibling].ref->get(), true);

                    if ((*graph)[sibling].own)
                        delete (*graph)[sibling].ref;

                    (*graph)[sibling].own = true;
                    (*graph)[sibling].ref = neg_constant;

                    move_to_left(parent, sibling, graph);
                }

                // change to addition
                OutEdgeIterator out_i, out_end;
                boost::tie(out_i, out_end) = boost::out_edges(vertex, *graph);
                BOOST_ASSERT(std::distance(out_i, out_end) == 1);

                auto value = boost::target(*out_i, *graph);
                auto e_old = boost::edge(parent, vertex, *graph);
                BOOST_ASSERT(e_old.second);

                auto e = boost::add_edge(parent, value, *graph);
                (*graph)[e.first].side = DataReference::RIGHT;

                boost::clear_vertex(vertex, *graph);
                boost::remove_vertex(vertex, *graph);

                // change parent from subtraction to addition
                if (!lhs)
                    (*graph)[parent].op = DataReferenceOperation::ADDITION;
                return true;
            }
            else
            {
                break;
            }

        case DataReferenceUnaryOperation::ONES_COMPLEMENT:
            std::cout << "no optimisation for ones complement\n";
            BOOST_ASSERT(0);
            break;
        case DataReferenceUnaryOperation::EQUAL:
        {
            OutEdgeIterator out_i, out_end;
            boost::tie(out_i, out_end) = boost::out_edges(vertex, *graph);
            BOOST_ASSERT(std::distance(out_i, out_end) == 1);

            auto value = boost::target(*out_i, *graph);
            auto e_old = boost::edge(parent, vertex, *graph);
            BOOST_ASSERT(e_old.second);

            auto e = boost::add_edge(parent, value, *graph);
            (*graph)[e.first].side = (*graph)[e_old.first].side;

            boost::clear_vertex(vertex, *graph);
            boost::remove_vertex(vertex, *graph);
            return true;
        }
        default:
            BOOST_ASSERT(0);
            break;
    }

    return false;
}

bool optimise_node(DataReference::VertexDescriptor parent, DataReference::VertexDescriptor vertex, DataReference::Graph *graph)
{
    using OutEdgeIterator = DataReference::GraphTraits::out_edge_iterator;
    OutEdgeIterator out_i, out_end;
    auto optimised = false;

    if ((*graph)[vertex].ref->constant())
    {
        return false;
    }

    // check if unary
    else if ((*graph)[vertex].op.which() == 1)
    {
        return optimise_unary(parent, vertex, graph);
    }

    else
    {
        for (boost::tie(out_i, out_end) = boost::out_edges(vertex, *graph); out_i != out_end;)
        {
            auto child = boost::target(*out_i, *graph);
            auto local_optimisation = false;

            if ((*graph)[child].ref->constant())
            {
                local_optimisation = promote_constant(parent, vertex, child, graph);
            }
            else
            {
                local_optimisation = optimise_node(vertex, child, graph);
            }

            if (local_optimisation)
            {
                boost::tie(out_i, out_end) = boost::out_edges(vertex, *graph);
                optimised = true;
            }
            else
            {
                ++out_i;
            }
        }
    }

    return optimised;
}

bool has_children(DataReference::VertexDescriptor node, DataReference::Graph *graph)
{
    using OutEdgeIterator = DataReference::GraphTraits::out_edge_iterator;

    OutEdgeIterator out_i, out_end;
    boost::tie(out_i, out_end) = boost::out_edges(node, *graph);
    return std::distance(out_i, out_end) != 0;
}

void operation_printer(DataReference::OperationType type)
{
    switch (type.which())
    {
        case 0:
        {
            auto op = boost::get<DataReferenceOperation>(type);

            switch (op)
            {
                case DataReferenceOperation::ADDITION:
                    std::cout << "DataReferenceOperation::ADDITION\n";
                    break;
                case DataReferenceOperation::SUBTRACTION:
                    std::cout << "DataReferenceOperation::SUBTRACTION\n";
                    break;
                case DataReferenceOperation::MULTIPLICATION:
                    std::cout << "DataReferenceOperation::MULTIPLICATION\n";
                    break;
                case DataReferenceOperation::DIVISION:
                    std::cout << "DataReferenceOperation::DIVISION\n";
                    break;
            }

            break;
        }
        case 1:
        {
            auto op = boost::get<DataReferenceUnaryOperation>(type);

            switch (op)
            {
                case DataReferenceUnaryOperation::NEGATIVE:
                    std::cout << "DataReferenceUnaryOperation::NEGATIVE\n";
                    break;
                case DataReferenceUnaryOperation::ONES_COMPLEMENT:
                    std::cout << "DataReferenceUnaryOperation::ONES_COMPLEMENT\n";
                    break;
                case DataReferenceUnaryOperation::EQUAL:
                    std::cout << "DataReferenceUnaryOperation::EQUAL\n";
                    break;
            }

            break;
        }
    }
}

class dfs_print_visitor: public boost::default_dfs_visitor
{
    using OutEdgeIterator = DataReference::GraphTraits::out_edge_iterator;
    OutEdgeIterator out_i, out_end;

public:
    dfs_print_visitor() { }

    template < typename Vertex, typename Graph >
    void discover_vertex(Vertex u, const Graph & g) const
    {
        if (g[u].ref->constant())
        {
            std::cout << "node (" << u << ") constant: " << g[u].ref->get() << "\n";
        }
        else if (has_children(u, &g))
        {
            std::cout << "node (" << u << ") op.which(): ";
            operation_printer(g[u].op);
            boost::tie(out_i, out_end) = boost::out_edges(u, g);
            std::cout << "\tchildren: " << std::distance(out_i, out_end) << "\n";
        }
        else
        {
            std::cout << "node (" << u << ") ASLR" << "\n";
        }
    }

    template < typename Vertex, typename Graph >
    void finish_vertex(Vertex u, const Graph & g) const
    {
        //std::cout << "finished vertex: " << u << "\n";
    }
};

DataReference::VertexDescriptor optimise_graph(DataReference::Graph *graph)
{
    using OutEdgeIterator = DataReference::GraphTraits::out_edge_iterator;
    OutEdgeIterator out_i, out_end;
    auto optimised = false;

    if ((*graph).vertex_set().empty())
        return nullptr;

    auto root = (*graph).vertex_set().front();

    if ((*graph)[root].ref->constant())
        return root;

    //dfs_print_visitor vis2;
    //boost::depth_first_search(*graph, boost::visitor(vis2).root_vertex(root));

    auto froot = boost::add_vertex(*graph);
    (*graph)[froot].ref = (*graph)[root].ref;
    (*graph)[froot].own = false;
    (*graph)[froot].op = DataReferenceUnaryOperation::EQUAL;

    auto e = boost::add_edge(froot, root, *graph);
    (*graph)[e.first].side = DataReference::LEFT;

    for (boost::tie(out_i, out_end) = boost::out_edges(froot, *graph); out_i != out_end;)
    {
        auto child = boost::target(*out_i, *graph);
        auto local_optimisation = false;

        if (optimise_node(froot, child, graph))
        {
            boost::tie(out_i, out_end) = boost::out_edges(root, *graph);
            optimised = true;
        }
        else
        {
            ++out_i;
        }
    }

    boost::tie(out_i, out_end) = boost::out_edges(froot, *graph);
    auto child = boost::target(*out_i, *graph);

    boost::clear_vertex(froot, *graph);
    boost::remove_vertex(froot, *graph);

    //dfs_print_visitor vis;
    //boost::depth_first_search(*graph, boost::visitor(vis).root_vertex(child));
    return child;
}

RelocationType op_to_reloc(DataReference::OperationType op, bool lhs)
{
    if (is_operation(op, DataReferenceOperation::ADDITION))
    {
        return RelocationType::ADD_SYMBOL;
    }

    if (is_operation(op, DataReferenceOperation::SUBTRACTION))
    {
        return lhs ? RelocationType::SUB_LHS_SYMBOL : RelocationType::SUB_RHS_SYMBOL;
    }

    if (is_operation(op, DataReferenceUnaryOperation::EQUAL))
    {
        return RelocationType::STORE_SYMBOL;
    }

    if (is_operation(op, DataReferenceUnaryOperation::NEGATIVE))
    {
        return RelocationType::SUB_RHS_SYMBOL;
    }

    BOOST_ASSERT(0);
    while(1);
}

RelocationList generate_relocs(DataReference::VertexDescriptor node, DataReference::Graph *graph, RelocationList& relocs)
{
    using OutEdgeIterator = DataReference::GraphTraits::out_edge_iterator;
    OutEdgeIterator out_i, out_end;
    boost::tie(out_i, out_end) = boost::out_edges(node, *graph);

    switch (std::distance(out_i, out_end))
    {
        case 1:
        {
            auto lhs = boost::target(*out_i, *graph);

            if (!has_children(lhs, graph))
            {
                if ((*graph)[lhs].ref->constant())
                {
                    BOOST_ASSERT(0);
                }

                Relocation reloc;
                reloc.type = op_to_reloc((*graph)[node].op, true);
                reloc.constant = 0;
                reloc.aslr_ref = (*graph)[lhs].ref;
                relocs.push_back(reloc);
            }
            else
            {
                generate_relocs(lhs, graph, relocs);
            }

            break;
        }
        case 2:
        {
            auto lhs = boost::target(*out_i, *graph);
            auto rhs = boost::target(*std::next(out_i), *graph);

            if (!has_children(lhs, graph) && !has_children(rhs, graph))
            {
                if ((*graph)[lhs].ref->constant() && (*graph)[rhs].ref->constant())
                {
                    BOOST_ASSERT(0);
                }

                else if ((*graph)[lhs].ref->constant())
                {
                    Relocation reloc;

                    reloc.type = op_to_reloc((*graph)[node].op, false);
                    reloc.constant = (*graph)[lhs].ref->get();
                    reloc.aslr_ref = (*graph)[rhs].ref;
                    relocs.push_back(reloc);
                }

                else if ((*graph)[rhs].ref->constant())
                {
                    Relocation reloc;

                    reloc.type = op_to_reloc((*graph)[node].op, true);
                    reloc.constant = (*graph)[rhs].ref->get();
                    reloc.aslr_ref = (*graph)[lhs].ref;
                    relocs.push_back(reloc);
                }
                else
                {
                    Relocation reloc;

                    reloc.type = op_to_reloc((*graph)[node].op, false);
                    reloc.constant = 0;
                    reloc.aslr_ref = (*graph)[rhs].ref;
                    relocs.push_back(reloc);

                    reloc.type = op_to_reloc((*graph)[node].op, true);
                    reloc.constant = 0;
                    reloc.aslr_ref = (*graph)[lhs].ref;
                    relocs.push_back(reloc);
                }
            }

            else if (has_children(lhs, graph))
                generate_relocs(lhs, graph, relocs);

            else if (has_children(rhs, graph))
                generate_relocs(rhs, graph, relocs);

            break;
        }
        default:
            BOOST_ASSERT(0);
            break;
    }

    return relocs;
}

RelocationList generate_relocs(DataReference::Graph *graph)
{
    BOOST_ASSERT(!(*graph).vertex_set().empty());
    auto root = (*graph).vertex_set().front();
    RelocationList relocs;
    return generate_relocs(root, graph, relocs);
}
} // anonymous

RelocationList create_relocation_for_aslr(AslrTable& aslr_table, DataReference *ref)
{
    // generate reloc information
    auto graph = ref->to_graph();

    auto filter = [&aslr_table, graph](const auto& ref)
    {
        for (const auto& entry : aslr_table)
        {
            if (entry.second.get() == (*graph)[ref].ref)
            {
                return true;
            }
        }

        return false;
    };

    auto begina = boost::make_filter_iterator(filter, graph->vertex_set().begin(), graph->vertex_set().end());
    auto enda = boost::make_filter_iterator(filter, graph->vertex_set().end(), graph->vertex_set().end());

    std::vector<boost::graph_traits<DataReference::Graph>::vertex_descriptor> aslr_verticesa(std::distance(begina, enda));
    std::copy(begina, enda, aslr_verticesa.begin());

    auto root = optimise_graph(graph);

    auto begin = boost::make_filter_iterator(filter, graph->vertex_set().begin(), graph->vertex_set().end());
    auto end = boost::make_filter_iterator(filter, graph->vertex_set().end(), graph->vertex_set().end());
    std::vector<boost::graph_traits<DataReference::Graph>::vertex_descriptor> aslr_vertices(std::distance(begin, end));
    std::copy(begin, end, aslr_vertices.begin());

    if (graph->vertex_set().size() > 3)
    {
        throw std::runtime_error("ASLR with too many vertices");
    }

    RelocationList relocs;
    generate_relocs(root, graph, relocs);

    delete graph;
    return relocs;
}

RelocationList create_relocation_for_aslr(AslrTable& aslr_table, DataRefPtr ref)
{
    return create_relocation_for_aslr(aslr_table, ref.get());
}
