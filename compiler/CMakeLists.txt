add_executable(roptool codegenerator.cpp datasection.cpp foldertarget.cpp program.cpp ropfunction.cpp ropfunctioncall.cpp ropsection.cpp roptool.cpp xmlgadget.cpp xmlgadgetmap.cpp xmltargetmanifest.cpp expressionreducer.cpp datareference.cpp relocation.cpp)

add_dependencies(roptool tinyxml2)
add_dependencies(roptool parser)

target_link_libraries (roptool ${Boost_LIBRARIES} parser tinyxml2)
