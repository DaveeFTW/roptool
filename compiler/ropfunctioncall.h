#ifndef ROPFUNCTIONCALL_H
#define ROPFUNCTIONCALL_H

// roptool
#include "gadgetmap.h"
#include "target.h"
#include "relocation.h"

// std
#include <memory>
#include <list>

class RopFunctionCall
{
    public:
        void setMap(GadgetMapPtr map);
        void setFunction(Function func);
        void setParameters(DataRefPtrList refs);
        size_t size(TargetPtr target);
        CompiledCode binary(TargetPtr target);

    private:
        GadgetMapPtr m_map;
        Function m_func;
        DataRefPtrList m_refs;
};

typedef std::shared_ptr<RopFunctionCall> RopFunctionCallPtr;
typedef std::list<RopFunctionCallPtr> RopFunctionCallPtrList;

#endif // ROPFUNCTIONCALL_H
