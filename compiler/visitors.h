class ExpressionVisitor
{
public:
	ExpressionVisitor() = default;
	virtual ~ExpressionVisitor() = default;

	virtual void operator()(roptool::ast::constant const& ast) { }
	virtual void operator()(roptool::ast::namespace_identifier const& ast) { }
	virtual void operator()(roptool::ast::operation const& ast) { boost::apply_visitor(*this, ast.operand_); }
	virtual void operator()(roptool::ast::unary_operand const& ast) { boost::apply_visitor(*this, ast.operand_); }
	virtual void operator()(roptool::ast::expression const& ast)
	{
		boost::apply_visitor(*this, ast.first);

		for (auto const& oper : ast.rest)
			(*this)(oper);
	}
};

class ArgumentVisitor
{
public:
	ArgumentVisitor() = default;
	virtual ~ArgumentVisitor() = default;

	virtual void operator()(roptool::ast::u64 const& ast) { }
	virtual void operator()(roptool::ast::u32 const& ast) { }
	virtual void operator()(roptool::ast::u16 const& ast) { }
	virtual void operator()(roptool::ast::u8 const& ast) { }
	virtual void operator()(roptool::ast::load_argument const& ast) { }
	virtual void operator()(roptool::ast::return_argument const& ast) { }
	virtual void operator()(roptool::ast::string_literal const& ast) { }
	virtual void operator()(roptool::ast::sized_expression const& ast) { boost::apply_visitor(*this, ast); }
	virtual void operator()(roptool::ast::expression const& ast) { }
};

class StatementVisitor
{
public:
	StatementVisitor() = default;
	virtual ~StatementVisitor() = default;

	virtual void operator()(roptool::ast::function_call const& ast) { }
	virtual void operator()(roptool::ast::return_statement const& ast) { }
};